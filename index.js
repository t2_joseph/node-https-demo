const express = require('express');
const https = require('https');
const fs = require('fs')

//Further reading - https://github.com/sevcsik/client-certificate-demo/blob/chapter-1/server.js
//requestCert: requests a client certificate from the user.
//rejectUnauthorized: accept requests with no valid certs. this is to handle invalid connections.
//ca: list of CA certificates that we consider valid. for now, we sign client certs with server key, so will be same as the server cert.

const opts = {
    key: fs.readFileSync('./server_key.pem'),
    cert: fs.readFileSync('./server_cert.pem'),
    requestCert: true,
    rejectUnauthorized: false,
    ca: [ fs.readFileSync('./server_cert.pem')]
}

const app = express();

//Unprotected landing page
//Everyone sees it whether they present a client cert or not
app.get('/', (req, res) => {
    console.log('Received a request');
	res.send('<a href="authenticate">Log in using client certificate</a>');
})

//Protected endpoint
app.get('/authenticate', (req, res) => {
    //get cert information from the https connection handle
    const cert = req.connection.getPeerCertificate();
    //req.client.authorized flag will be true if cert is valid and issued by a CA white-listed earlier in opts.ca
    if(req.client.authorized){
        res.send(`Hello ${cert.subject.CN}, your certifiate was issued by ${cert.issuer.CN}`)
    } else if(cert.subject) { //wrong certificate
        res.status(403).send(`Sorry ${cert.subject.CN}, certificates from issuer ${cert.issuer.CN} are not welcome here.`)
    } else { //no certificate
        res.status(401).send(`Sorry you need to provide a client certificate to continue`)
    }
})

console.log(fs.readFileSync('./server_key.pem'))

https.createServer(opts, app).listen(9999)
